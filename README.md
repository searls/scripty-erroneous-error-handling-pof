# Scripty erroneous error code handling proof of concept

To run:

```sh
npm i
npm test
```

Please notice the following:

- The `test` script runs `npm run one` and `npm run two`, and then
  runs a short `echo` command.  In addition the test command echoes
  the var `$?`, which contains the error code of the last run command.
- Both scripts, `one` and `two` output a block of text with their
  respective number and they both do `exit 1` right afterwards.

Under the above conditions, I would expect the error code to equal 1,
however it equals 0 because the last `echo` resets it to 0.

BTW the fact that the error code is that of the last command I did not
realize, I thought it was a 0 under all circumstances even if the
scripts failed, but it actually simply retains the status of the last
exit code in the script.

If you remove that last `echo` command the outputted error code will
in fact be 1, according to that of the last command run.

The above means that if one runs several test scripts, the CI will not
realize that a command actually failed, because what actually matters
is the status code of the last test script that runs, not of the
entire test suite.

Feel free to play around a bit more with this. :)
